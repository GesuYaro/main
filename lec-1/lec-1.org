#+SETUPFILE: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup
#+LANGUAGE: ru
#+OPTIONS: ':t
#+OPTIONS: toc:nil
#+OPTIONS: title:t  
#+latex_header: \usepackage[a4paper]{geometry}
#+latex_header: \usepackage[utf8x]{inputenc}
#+latex_header: \usepackage[T2A]{fontenc}
#+latex_header: \usepackage[russian,english]{babel}
#+latex_header: \usepackage{fullpage}



#+TITLE:  Лекция 1. Основы архитектуры вычислительных систем

  #+BEGIN_COMMENT
    
  In this course we are studying low-level programming languages in the
  context of complex computer systems.
  Naturally we start with introducing the notion of systems, computer systems
  and why is the system approach important for programmers and computer
  engineers alike.

  Note that this is not a course in systems theory so a lot of concepts can be
  expanded or made more precise.

  There are two main sources of information for this course:
    
  - The systemic aspect is well described in the book "Principles of Computer System Design: an Introduction". It is used in MIT to teach a two-semester course in computer systems.
  - The programming aspect is described in the book "Low-level programming; C, Assembly and Program Execution".

    
  #+END_COMMENT
  -----

* Материалы лекции
  :PROPERTIES: 
  :UNNUMBERED: t
  :END:
  
    #+BEGIN_COMMENT
    In every chapter I will provide references to external sources. You can pass
    the course without studying them, but I encourage you to read them because
    they extend the course and place it in a broader context.

    For today's part the references are as follows:
    
    #+END_COMMENT
    
  - "Low-level programming": глава 1 целиком.
  - "Principles of Computer System Design: an Introduction":
    - Глава 1 "Systems" целиком стр. 2--41.
    - Глава 2, "§2.1 The Three Fundamental Abstractions" стр. 45--59.

  -----

* Системы

  - *Система* это набор связанных компонентов, обладающий свойствами, которыми компоненты не обладают по отдельности.

** Примеры

   - Автомобиль :: на запчастях нельзя ездить, но на автомобиле можно.
   - Человек :: отдельные клетки неразумны, человек разумен.
   - Компьютер :: на процессоре нельзя вычислять, на системном блоке можно.

     #+BEGIN_COMMENT

     In this course we are interested in a particular kind of computer
     systems. To provide context, let us start by giving a precise
     definition of a system in general, not necessary a computer system.

     What is a system? The most generic definition would be: a collection of
     interacting components.

     The defining characteristic of systems is that interactions between
     their components result in new properties of the system as a whole; we call such properties /systemic/. 

     For example, a car has a systemic property: it can carry people and objects around.
     None of its part can transport you like the car does.
     Moreover, when the car is fully assembled but its engine is not started, the car stays in place and can not move.
     That is because only interaction between the parts of a car allow it to move, providing you with a useful functionality.

     A human being is also different from an unanimated corpse. Our cells are
     interacting giving us an ability to walk, think, breathe and, generally,
     making us live and sentient. These are our systemic properties.


     A computer can not perform any work unless powered up and turned on.
     The ability to perform computations are its systemic property, born from
     interactions of CPU, memory, storage, buses and other hardware
     components.
         
     This is what we mean when we say, that a system is "more than a sum
     of its parts". A system is a sum of its parts, and also interactions between them.

    
     #+END_COMMENT
   
   -----

** Окружение, над- и подсистемы

   - Система работает в /окружении/.
     - Машина взаимодействует с объектами вокруг себя и водителем.

       #+BEGIN_COMMENT
       Systems exist in an /environment/. For example, a car interacts with the
       driver and the road, the trees around it, other cars. Every of us, humans,
       interacts with other humans, with sounds and visuals, with the air that we
       breathe and the food that we consume. A computer interacts with other
       devices, with human through screens, keyboards and other interfaces and so
       on.

       #+END_COMMENT


   - Система может быть частью большей системы.
     - Бортовой компьютер это часть машины, но и он состоит из подсистем.
       #+BEGIN_COMMENT
       Generally speaking, an environment consists of everything of interest
       placed around the system: noises, sources of information, objects the
       system may be interacting with, other systems, or a parent super-system, of
       which our system of interest is a part. Usually it mixes an organized part
       and a chaotic part. Note that the environment only includes such objects
       that can potentially interact with the system. It it not useful to include
       in the car's environment blue whales, sound waves, bacteria or asteroids.

       #+END_COMMENT

   - Любая система связана с окружением через /интерфейс/.
     - Руль, рычаги, переключатели, колёса...
       #+BEGIN_COMMENT
       Interactions with systems happen through their /interface/. For a car their
       controls, as well as their wheels are parts of interface. For us, humans, our
       eyes, ears, skin are parts of our interface. However, note, that depending
       on the system's function the interface can vary: if we consider a verbal
       conversation between two humans, their interface is a spoken word; however,
       when these humans are dancing together, the interfacing happens through
       visual contact and touch.

       The notion of interface is closely related to the border between system and
       its environment. The borders of a system are usually blurred. We can
       postulate that the parts of system that can be attributed to both the
       system itself and its environment are the border.
       #+END_COMMENT


   -----

** Вычислительные системы
   В *вычислительных* системах мы через интерфейс *наблюдаем* процессы внутри и *интерпретируем результаты* --- так и происходят вычисления.


   #+BEGIN_COMMENT

   We understand what systems are now. But what are computer systems? We are
   not studying cars or human biology after all.

   Here is what makes computer systems distinct --- or not so much. 
   We can peek through their interfaces and observe the processes inside; then
   we /interpret/ our observations. And this is what constitutes /the
   computations/ --- our interpretation of what we observe in systems.

   Why did I tell that the computer systems are /not so much distinct/? Because our interpretation is a crucial part of the process.
   To demonstrate this I will provide some examples of computer systems where
   some computation is happening, but not in electric circuits.

   #+END_COMMENT
*** Природные вычислительные системы

    - random.org :: случайные числа из атмосферного шума;

      #+BEGIN_COMMENT
      You all know very well by now that computers are not good with random
      numbers. Unless they incorporate a specific hardware component to generate
      random numbers, they can not achieve any true non-determinism, only imitate
      it through clever mathematical trickery. But random numbers are extremely
      useful, especially in cryptography.

      There is a service that you can access at random.org. It provides good
      quality random numbers by using electromagnetic noise from the atmosphere
      as a seed for the random number generator.
      Using such numbers in cryptography is much more sane than those generated
      by a traditional computer.

      We can say that the part of random.org that performs computations is the
      atmosphere of Earth itself. This may seem quite unorthodox: it is not man-made. However,
      observing this atmospheric noise and interpreting our observations, then
      plugging it into a more conventional computer system is providing us with
      numbers with very particular properties. What is it, if not a computing process?

      #+END_COMMENT
    - звёзды-пульсары :: точно отсчитывают время;
      #+BEGIN_COMMENT
      Another example are pulsars. Pulsars are stars at late stages of their life,
      which are emitting bursts of light with high frequency. They are doing it in
      a very stable way and thus can be used as clock: we count the bursts and
      know how many milliseconds have passed. Counting time is also computation.
      #+END_COMMENT

    - колония муравьёв :: оптимизирует пути до еды;
    #+BEGIN_COMMENT
    The third example are ant colonies. They are searching for food in a process
    that can be approximately described as a random walk. As they walk, they mark their steps with a scent.
    When an ant worker reaches the food or other interesting resource it goes
    back to the ant colony in exactly the same way. Thanks to the smelly
    footprints it can go back following exactly the same route that led the
    little ant to food.
    However this path can be very far from optimal: for example, it may contain loops.
    In order to optimize it the ants use their capacities to recognize stronger and weaker smells and thus get a sense of where the route is going, and which routes are used by more ants.
    This knowledge seems enough to be able to shorten the paths substantially.

    So when you put a piece of food in the proximity of ant colony, and when
    they find it, by observing ants you may get a somewhat optimal solution to a
    problem of finding path from ant colony to the food.

    This is a computation that does not look any different from those performed
    by computers, and actually ant colonies have inspired some path finding
    algorithms.
    #+END_COMMENT
     
    #   - [[https://ru.wikipedia.org/wiki/%D0%94%D0%9D%D0%9A-%D0%BA%D0%BE%D0%BC%D0%BF%D1%8C%D1%8E%D1%82%D0%B5%D1%80][ДНК-компьютер]] :: генерация множества решений + фильтрация.

    #+BEGIN_COMMENT    
    To sum it up, computations can be performed in a lot of ways, and they are
    closely tied up with interpretation. Sometimes nature performs computations
    without computers, and we are saying so because we are interpreting natural
    processes as computations.
    #+END_COMMENT
    
    -----

** Программы как системы
   
   - Выполняющаяся программа --- часть программно-аппаратной системы.

    #+BEGIN_COMMENT

    Now back to the computer systems. Our main interest in this course is programming. What is relationship between programming and computer systems?

    When we program, the outcome of our work is a program, more precisely, its
    source code. It may be executed through interpretation, it may be compiled and
    then executed in a different form --- it does not matter.

    #+END_COMMENT
   - Исходный код --- не система.
     
    #+BEGIN_COMMENT
    What matters is that the source code is not a system. Systems are alive, their 
    parts are interacting and new properties emerge from these interactions. Source code is dead.
    It is like a manual on how to assemble a piece of furniture, addressed to someone who is going to perform the actual work.

    What is our interest in systems then? When we launch program and it is being executed, it becomes a part of a system incorporating hardware and software parts.
    It is impossible to understand its behavior without thinking about hardware, and software, and their interaction.

    #+END_COMMENT


    #+ATTR_HTML: :border 0 :frame none :rules none
    | Исходный код                | Программа                 |
    |-----------------------------+---------------------------|
    | чертёж самолёта             | летящий самолёт           |
    | инструкция по сборке мебели | сборщик в процессе работы |
    
    
    #+BEGIN_COMMENT
    If we draw an analogy between, on one hand, the source code and the program being executed, and something else, then
    the source code relates to the program in course of execution as 
    a manual on how to assemble a piece of furniture relates to a system of furniture assembler, instruction manual and furniture parts; this system is performing work on assembling the furniture.

    #+END_COMMENT

    -----
** Декомпозиция систем
   Как минимум два /разных/ способа разделить систему на части:

   - Структурный :: из чего собрана?
     
      #+BEGIN_COMMENT
      All right, now we want to elaborate how to identify the parts that
      constitute a system. Given a system, possibly very complex, how to decompose
      it in parts? It is not an easy question to answer: there are several ways on
      decomposing a system into parts.

      The first, and most obvious way, is to decompose the system spatially. When
      a system is composed from parts like a house is composed of bricks, we are
      talking about its structural decomposition; the system is a whole, and the smaller pieces are its parts.
#+END_COMMENT

   - Функциональный :: какая часть реализует какую функцию?

      #+BEGIN_COMMENT
      The second, and often more useful way, is to decompose the system
      functionally. What does each part of system do? This is not the same as
      structurally decomposing system, because we only care about the
      functionality, the roles of parts of the system.

      #+END_COMMENT
      
*** Пример: ножницы

      #+BEGIN_COMMENT
      Take scissors as an example of a system. It is able to cut paper better than
      a simple knife because of its systemic properties.
      #+END_COMMENT
      
    - три структурные: два куска металла и винт.


      #+BEGIN_COMMENT
      Structurally scissors are composed of two pieces of metal and a screw that connects them together. 
      #+END_COMMENT
      
    - две функциональные части: держатели и режущая;
      
      #+BEGIN_COMMENT
      Functionally, the same scissors consist of two parts: one is used to hold
      them, whereas another cuts paper. Not only do scissors have more structural
      parts than functional, we can not map structural parts to functional parts
      trivially.

      In a way, functional parts are /roles/ that people play. Consider a system
      consisting of people working on the same project. Structurally, every person
      is a part of such system. Functionally, the parts of this system are /roles/:
      programmers, designers, managers, testers etc. One of the differences
      between them is that one person can play several roles (e.g. be a programmer
      and project manager, or programmer and graphics designer).

      Functional way of viewing systems is considered much more efficient for
      engineering computer systems including programs.
     
      #+END_COMMENT
    -----

*** Курсор --- структура или функция?
    
    /Курсор/ --- функциональный компонент.

    Может быть мышь, планшет --- или программа, управляющая курсором.
    
    #+BEGIN_COMMENT
    To get accustomed to structural and functional decompositions let us study
    some examples: this time, from computer systems and programming.

    The first example is cursor; we all see it constantly on computer screen
    as we point at things, drag or otherwise interact with them.
    Cursor is a functional component, not a structural one.
    It is not equivalent to mouse, graphical tablet or any other
    device: a program can take control over the cursor and move it around.
      
    #+END_COMMENT
    -----

*** Виртуальная память --- структура или функция?

    - Существуют ли адреса, которые не соответствуют физической памяти?
      
      #+BEGIN_COMMENT

      As you know from your previous studies, there are a lot of programs trying
      to execute simultaneously on your computer. They coexist and are isolated
      from one another through virtual memory.

      Consider virtual memory. Is it a structural part of computer system or a
      functional one?

      The function of virtual memory is to store data. The virtual memory space
      itself consists of areas of forbidden addresses, files, mapped from hard
      drive, anonymous pages and so on. The virtual memory is backed by the
      physical memory, storage (in case of memory swapping or mapping files)
      and operating system, which ensures transparent operation with it.

      It does not seem possible to divide computer system into structural parts
      in a way that isolates one virtual memory space from all others, so it is
      not a structural component.
              
      #+END_COMMENT
    -----
*** Процесс --- структура или функция?

    - В помощь: можно ли разделить систему в пространстве так, чтобы один процесс был отделён ото всех остальных?

      #+BEGIN_COMMENT
      Another example is a process --- we have all seen a list of processes in a task manager.
      A similar argument is applicable to it: we can not divide computer on parts structurally to isolate one process.
      A process contains a virtual address space too. So a process is also a functional component.
      #+END_COMMENT
    -----
*** Хранилище --- структура или функция?

    - ramfs;
    - распределённые файловые системы.
    
      #+BEGIN_COMMENT
      The final example is a storage  --- its name alone suggests its function and hence that it is a functional component.
      It can be implemented as a piece of hardware, but the data can be stored in a distributed filesystem, in a distributed database, in cloud.
      In cloud storage it is not clear where a specific piece of data is stored due to factors like replication blurring it.
      #+END_COMMENT
    -----
**  Сложность систем


      #+BEGIN_COMMENT
      Now we know what computer systems are, we have studied how to decompose
      them into parts in a structural or functional way; we have provided some
      examples of their functional components.
      
      Modern computer systems have a lot of components both structurally and
      functionally; it suggests that they are, in layman's terms, complex. But
      what is the complexity we are talking about here? And what is complexity
      at all?

      #+END_COMMENT

   Сложность это главная проблема при создании систем.

      #+BEGIN_COMMENT
      There are different approaches to defining complexity, and some work better depending on the system.
      #+END_COMMENT
      
      
   1. Как сложно описать систему? Иногда измеряется в битах.

      #+BEGIN_COMMENT
      In the first approach we postulate that a system is complex when it is
      difficult to describe. As an example, imagine we describe the system
      through programming and we have fixed a programming language to do that.
      Then a measure called Kolmogorov complexity shows the minimal length of a
      computer program that can possibly describe the system.
      #+END_COMMENT
      
       Keywords: Information, entropy, algorithmic complexity, Fisher's
      information, Renyi's entropy, code length (Hamming, Shennon-Fano),
      Chernov's information, Lempel-Ziv complexity, Kolmogorov complexity.
       
   2. Как сложно создать систему?

      #+BEGIN_COMMENT
      In the second approach we postulate that a system is complex when it is
      difficult to create. Creation can be measured in energy or money, but also a working computer system requires performing computations.
      Systems that require more computations, or more computational resources like memory for temporary data, are then deemed complex.

      A well known way of describing complexity in this manner is using algorithmic, data or communication complexity, usually in asymptotic form.
      Drawing a parallel with Kolmogorov complexity, there is a measure called logical depth that corresponds to the fastest computer program able to  describe the system.
      #+END_COMMENT
      
      Keywords: Algorithmic space/time complexity, logical depth, termodynamic depth, crypticity.

   3. Насколько система регулярна?

      #+BEGIN_COMMENT
      In the third approach the system is complex if it is irregular. As an
      example, imagine a map-reduce engine. Such an engine takes a huge problem
      and a huge chunk of data. Then it splits the problem into smaller
      independent subproblems, each using a portion of data. The subproblems are
      distributed over a number of computational nodes, then the engine
      aggregates their solutions into the final answer to the huge problem.

      Is this system complex? It may seem huge, and we can have thousands of
      computing nodes, but all these nodes are the same; they fit the same
      description. Being able to describe multiple parts of the system in the
      same way decreases its complexity. Think about it when you plan your
      programs, because reusing code usually makes them less complex.

      #+END_COMMENT
      
       Keywords: entropy.
      
   -----

** Признаки сложной системы

      #+BEGIN_COMMENT
      While computing an exact metric of complexity is usually impossible, there
      are some signs that suggest that the system is complex according to one of
      these metrics.
      #+END_COMMENT
      
   1. Много частей.
   2. Много связей.
   3. Много нерегулярностей.
   4. Большое описание.
   5. Большая команда работает над системой.


   -----
* Борьба со сложностью

      #+BEGIN_COMMENT
      So, for practical reasons we need to build computer systems that solve
      complex problems. But such systems tend to be complex themselves. It makes
      them harder to build, maintain, and develop. Without a right approach we
      may not even be able to complete the construction of the system because
      modifying it will become unbearably difficult. For instance, if all parts
      are tightly interdependent, modifying one part may produce a ripple effect
      that would force us to modify all parts that depend on it.

      #+END_COMMENT

      Эмпирически найдены подходы, которые часто эффективны.
    
  - Модульность ::  делаем систему по кусочкам, затем собираем как конструктор.

      #+BEGIN_COMMENT
      Modularity implies that we dissect the system on smaller parts that are easier to digest; then we repeat the dissection until we face simple challenges.
      We have to be able to develop modules in isolation, without touching other modules.
      Then we can ascend the hierarchy, building more complex blocks with the modules that we already have.
      #+END_COMMENT
    
  - Абстракция :: забываем устройство модулей, описываем только поведение.
      #+BEGIN_COMMENT
      Abstraction goes together with modularity and makes it even more useful. If modularity is just about dissection of system into independent parts, abstraction is about forgetting and simplifying our idea about these parts.
For example, when we communicate between two programs through Internet, we are
usually not thinking about how electrical signals pass through wires, and that
the packets may get corrupted and we have to correct errors in them. This is
achieved by abstracting the exchanging mechanism so that

      #+END_COMMENT
    

  -----
** Три типа функциональных компонентов
#+BEGIN_COMMENT

Earlier we have provided examples of computer systems where the computation is performed in an unorthodox way, such as ant colonies.
It suggests that in reality we can make computer systems out of virtually anything, and only our creativity is the limit here.
However, this is not a very useful statement because we need some guidelines to start exploring the vast chaotic space of possible computer systems.
#+END_COMMENT

   #+BEGIN_COMMENT
   As diverse as computer systems can be, once we pick a typical computer system
   apart and perform its functional decomposition we will see that its components
   are usually falling into one of three categories.
   #+END_COMMENT
        - Память (memory) :: хранение данных.
        - Исполнитель (interpreter) :: реакция на события, выполнение команд.
        - Транспорт (communication link) :: связь между компонентами.
   #+BEGIN_COMMENT
   Before we explain why do we care about these components, let us provide some examples for each of them.
   #+END_COMMENT
   -----
*** Примеры памяти
#+BEGIN_COMMENT
Variants of memory may be the easiest to recall. Memory can be implemented in a
variety of ways, some of which are mostly hardware, others are mixed
hardware-software solutions.

Here are some examples of memory in computer systems:

#+END_COMMENT
    - Триггеры
    - Регистры
    - Оперативная память
    - Кэши
    - Виртуальная память
    - HDD/SDD
    - RAID
    - Базы данных
    - Файловые системы
    - Облачное хранилище

    -----

*** Примеры исполнителей

#+BEGIN_COMMENT
Interpreters are active or reactive components of computer systems.

Here are some examples of interpreters:
#+END_COMMENT

    - Процессор
    - Контроллеры
    - Интерпретаторы и компиляторы
    - Виртуальные машины
    - Word
    - Браузеры
    - Игры

    -----

*** Примеры транспорта

#+BEGIN_COMMENT
Finally, communication link connects other components allowing them to interact. Multiple components can be connected; in this case transport has to perform some kind of routing to transport data from sender to a correct receiver.

Here are some examples of communication links:
#+END_COMMENT
    - Разные виды кабелей
    - Wi-Fi
    - USB
    - Internet
    - Telegram
    - email
    - socket
    - pipe
    - файл-буфер...

    -----
*  Конструктор вычислительных систем

#+BEGIN_COMMENT
We care about these three types of functional components. Why? To give you a
taste, no matter how memory is implemented, be it a database or a hard disk
drive, there are a number of universal memory characteristics such as atomicity
of reads and writes, or latency, or coherence between reads and writes. These
characteristics pose certain challenges, like decreasing memory latency, and
there are ways of dealing with such problems which are independent from the
specific memory implementation.

These components also provide us with a useful language to describe computer systems.
We can represent a system with a needed level of detail as a combination of memories and interpreters connected by transports.
#+END_COMMENT

  Посмотрим на комбинации трёх функциональных компонентов.
  Помните, что блоки на схемах это функциональная декомпозиция: за каждым элементом или связью может скрываться сложная, многослойная структура. 

** Архитектура фон Неймана

   [[./img/von-neumann-pure.svg]]

#+BEGIN_COMMENT

#+END_COMMENT
   -----
** Архитектура фон Неймана с регистрами

   [[./img/von-neumann-regs.svg]]

#+BEGIN_COMMENT

#+END_COMMENT
** сеть между двумя компьютерами

   [[./img/network.svg]]
    
   #+BEGIN_COMMENT

#+END_COMMENT

   Транспорт может быть очень сложным: процессы поиска пути для пакетов, повторной посылки потерянных пакетов, несколько уровней протоколов (см. [[https://ru.wikipedia.org/wiki/%D0%A1%D0%B5%D1%82%D0%B5%D0%B2%D0%B0%D1%8F_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C_OSI][модель OSI]]).
   -----

#+BEGIN_COMMENT

#+END_COMMENT

   - мультипроцессорная система с общей памятью;
   - компилятор (в случае когда модели памяти для языков одинаковые). 

   [[./img/compiler.svg]]

#+BEGIN_COMMENT

   Низкий уровень --- когда два исполнителя похожи. Байткод JVM низкий уровень для JVM, по сравнению с машинными кодами --- нет. Правда, JVM можно сделать в hardware.
#+END_COMMENT
   -----

    
*  Модель Intel 64


#+BEGIN_COMMENT

#+END_COMMENT
** Добавление прерываний

   Неинтерактивность: система простаивает при работе с медленными внешними устройствами.

   [[./img/von-neumann-regs-int.svg]]


#+BEGIN_COMMENT

#+END_COMMENT
   -----

** Усложнение из жадности
   
   Хотим больше производительности --- усложняем систему, а ещё и в худшем случае понижаем производительность.

   Пример: два поезда и длинный узкий каньон ("Principles of computer system design" §1.2.2).
    
   Память --- медленная. Добавим регистры, кэш. Медленная внешняя память.

   [[./img/von-neumann-complete.svg]]


#+BEGIN_COMMENT

#+END_COMMENT
   -----


** Стек

   - изолировать куски кода в процедурах
   - сохранять контекст выполнения и потом возвращаться.
     
     Добавился аппаратный стек (но в рамках той же линейно адресуемой памяти)


   Стек это функциональный компонент или структурный?


#+BEGIN_COMMENT

#+END_COMMENT
   -----
** Виртуализация, мультизадачность


#+BEGIN_COMMENT

#+END_COMMENT
   - виртуализировать процессор;
   - изолировать процессы друг от друга;
   - не позволять выполнять всем опасные инструкции (привилегированный режим).


   [[./img/final-approximation.svg]]


   -----
* TODO Итоги


  -----
* В следующей лекции
  концентрируемся на абстракции *исполнитель*, узнаём понятие *модели вычислений* и их примеры.

  - узнаем многое, практически применимое в других языках;
  - while/if, вызов процедур, объекты и классы &mdash; капля в океане способов организации вычислений;
  - есть много специализированных инструментов;
  - как устроена программа на ассемблере (GNU\Linux, nasm, AMD64);
  - как легко сделать самые сложные части первой лабораторной с помощью конечных автоматов.
