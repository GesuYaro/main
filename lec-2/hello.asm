global _start
section .data
message: db  'hello, world!', 10 ; ASCII
section .text
exit:     mov     rax, 60          ; 'exit' syscall number
          xor     rdi, rdi
          syscall
                                   ;  ret = pop rip 
_start:   mov     rax, 1           ; 'write' syscall number
          mov     rdi, 1           ; stdout descriptor
          mov     rsi, message     ; string address
          mov     rdx, 14          ; string length in bytes
          syscall
          call exit



  ;;         calls: rdi, rsi, rdx, rcx, r8, r9
  ;;  system calls: rdi, rsi, rdx, r10, r8, r9
